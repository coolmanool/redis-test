import random
import redis
import redis_lock
import string
import time
from work_with_rooms import find_room, refresh_room_state, delete_room


###############################################
# ##--------------UTILS-------------------### #
###############################################


def generate_random_str(length):
    return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(length))


def get_cur_time_ms():
    return int(round(time.time() * 1000))


# create rooms with pointed users
def create_rooms(r, count_rooms, count_ssids):
    room_names = []
    for i in range(count_rooms):
        rnd_str = generate_random_str(8)
        room_name = 'R_{}_{}'.format(rnd_str, rnd_str)
        lock = redis_lock.Lock(r, room_name)
        if lock.acquire(blocking=True):
            try:
                ssids = {'u_{}'.format(ssid): 'pointed_{}'.format(get_cur_time_ms() + 15000) for ssid in range(count_ssids)}
                r.hmset(room_name, ssids)
            finally:
                lock.release()
        room_names.append(room_name)
    return room_names

###############################################
# ##--------------TESTS-------------------### #
###############################################

###############################################
# ##----------CREATE & JOIN---------------### #
###############################################


# just create room
def test_1(r):
    out = find_room(r, '1')
    assert r.keys() == []
    assert out['mode'] == 'create'
    assert len(out['room_name']) > 20


# create room when other rooms exist (count > 10)
def test_2(r):
    count_rooms = 15
    create_rooms(r, count_rooms, 4)

    out = find_room(r, '5')
    assert len(r.keys()) == count_rooms
    assert out['mode'] == 'create'
    assert len(out['room_name']) > 20


# join room
def test_3(r):
    count_rooms = 1
    free_room_name = create_rooms(r, count_rooms, 3)[0]

    out = find_room(r, '5')
    assert len(r.keys()) == count_rooms
    assert out['mode'] == 'join'
    assert free_room_name == out['room_name']


# join room if count of existing room > 10
def test_4(r):
    count_rooms = 10
    create_rooms(r, count_rooms, 4)
    count_free_rooms = 1
    free_room_name = create_rooms(r, count_free_rooms, 3)[0]

    out = find_room(r, '5')
    assert len(r.keys()) == count_rooms + count_free_rooms
    assert out['mode'] == 'join'
    assert free_room_name == out['room_name']


###############################################
# ##----------------REFRESH---------------### #
###############################################


# refresh if room name is None
def test_5(r):
    refresh_room_state(r, '5', None, ['1', '2', '3'])

    assert len(r.keys()) == 0


# refresh: pointed update to real
def test_6(r):
    count_rooms = 1
    room_name = create_rooms(r, count_rooms, 4)[0]

    refresh_room_state(r, '5', room_name, ['1', '3'])
    d = r.hgetall(room_name)
    assert len(r.keys()) == count_rooms
    assert d[b'u_0'].decode().split('_')[0] == 'pointed'
    assert d[b'u_1'].decode().split('_')[0] == 'real'
    assert d[b'u_2'].decode().split('_')[0] == 'pointed'
    assert d[b'u_3'].decode().split('_')[0] == 'real'


# refresh if pointed expired
def test_7(r):
    count_rooms = 1
    room_name = create_rooms(r, count_rooms, 3)[0]

    time.sleep(20)
    refresh_room_state(r, '5', room_name, ['5'])
    assert len(r.keys()) == count_rooms
    assert r.hlen(room_name) == 1
    assert r.hkeys(room_name) == [b'u_5']


# refresh if ssid = 4 and pointed not expired
def test_8(r):
    count_rooms = 1
    room_name = create_rooms(r, count_rooms, 4)[0]

    refresh_room_state(r, '5', room_name, ['5', '6', '7', '8'])
    assert len(r.keys()) == count_rooms
    assert r.hlen(room_name) == 8


# check room expired
def test_9(r):
    count_rooms = 1
    room_name = create_rooms(r, count_rooms, 4)[0]

    refresh_room_state(r, '5', room_name, ['1'])
    time.sleep(20)
    assert len(r.keys()) == 0


# check: old real deleted, pointed keep alive
def test_10(r):
    count_rooms = 1
    room_name = create_rooms(r, count_rooms, 4)[0]

    refresh_room_state(r, '5', room_name, ['0', '1', '2'])
    refresh_room_state(r, '5', room_name, ['5', '6', '7', '8'])
    assert len(r.keys()) == count_rooms
    assert set(r.hkeys(room_name)) == set([b'u_5', b'u_6', b'u_7', b'u_8', b'u_3'])
    assert r.hget(room_name, 'u_3').decode().split('_')[0] == 'pointed'
    assert r.hget(room_name, 'u_5').decode().split('_')[0] == 'real'


###############################################
# ##----------------DELETE----------------### #
###############################################


# just delete room
def test_11(r):
    count_rooms = 1
    room_name = create_rooms(r, count_rooms, 4)[0]

    delete_room(r, '5', room_name)
    assert len(r.keys()) == 0


# delete room if no room
def test_12(r):
    delete_room(r, '5', 'xxx')
    assert len(r.keys()) == 0


def run(func):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    r.flushall()
    func(r)


if __name__ == '__main__':
    run(test_10)
