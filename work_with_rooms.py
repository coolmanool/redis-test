import random
import redis_lock
import string
import time


def generate_pwd(length):
    return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(length))


def get_cur_time_ms():
    return int(round(time.time() * 1000))


def find_room(r, ssid):
    count = 10
    pos = 0
    while True:
        rooms = r.scan(pos, 'R_*', count)
        for room_name in rooms[1]:
            users = r.hscan(room_name, 0, 'u_*', count)
            if len(users[1]) < 4:
                lock = redis_lock.Lock(r, room_name)
                if lock.acquire(blocking=True):
                    try:
                        r.hset(room_name, 'u_{}'.format(ssid), 'pointed_{}'.format(get_cur_time_ms() + 30000))
                    finally:
                        lock.release()
                return {'mode': 'join', 'room_name': room_name}
        if pos == 0:
            room_name = 'R_{}_{}'.format(get_cur_time_ms(), generate_pwd(16))
            return {'mode': 'create', 'room_name': room_name}


def refresh_room_state(r, ssid, room_name, room_ssids):
    if room_name is None:
        return

    lock = redis_lock.Lock(r, room_name)
    if lock.acquire(blocking=True):
        try:
            data = r.hgetall(room_name)
            new_data = {}
            for room_ssid in room_ssids:
                new_data[('u_{}'.format(room_ssid)).encode()] = b'real'
            new_data_keys = new_data.keys()
            for key, value in data.items():
                user_state = value.split(b'_')
                if user_state[0] == b'pointed' and key not in new_data_keys and get_cur_time_ms() < int(user_state[1]):
                    new_data[key] = value
            r.delete(room_name)
            r.hmset(room_name, new_data)
            r.expire(room_name, 15)
        finally:
            lock.release()


def delete_room(r, ssid, room_name):
    lock = redis_lock.Lock(r, room_name)
    if lock.acquire(blocking=True):
        try:
            r.delete(room_name)
        finally:
            lock.release()
