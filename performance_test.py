import redis
import time
from multiprocessing import Process
from work_with_rooms import find_room, refresh_room_state, delete_room


class Room:
    rooms = []

    def __init__(self, name):
        self.name = name
        self.users = []

    @classmethod
    def get_room(cls, room_name):
        for r in cls.rooms:
            if r.name == room_name:
                return r


class User:
    def __init__(self, ssid, master, room):
        self.ssid = 'u_{}'.format(ssid)
        self.master = master
        self.room = room


def start(ssid):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    room = find_room(r, ssid)
    if room['mode'] == 'create':
        rm = Room(room['room_name'])
        Room.rooms.append(rm)
        user = User(ssid, True, rm)
        rm.users.append(user)
    elif room['mode'] == 'join':
        try:
            rm = Room.get_room(room['room_name'])
            user = User(ssid, False, rm)
            rm.users.append(user)
        except Exception as e:
            print(e)


def main():
    processes = []
    for i in range(10000):
        p = Process(target=start(i))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()


if __name__ == '__main__':
    main()
