import redis
import redlock
from multiprocessing import Process


r = redis.StrictRedis(host='localhost', port=6379, db=0)
dlm = redlock.Redlock([{'host': 'localhost', 'port': 6379, 'db': 0}, ], 10)


def process(pid):
    for i in range(10):
        # r.set('locker', pid)
        # while int(r.get('locker')) != pid:
            # print('eee {}-{}'.format(pid, int(r.get('locker'))))
            # r.set('locker', pid)
        lock_set = dlm.lock('value_set', 5000)
        lock_incr = dlm.lock('value_incr', 5000)
        if lock_set is not False:
            print('lock_set ok')
            value = r.get('value_set')
            r.set('value_set', int(value) + 1)
            dlm.unlock(lock_set)
        if lock_incr is not False:
            print('lock_incr true')
            r.incr('value_incr')
            dlm.unlock(lock_incr)


def main():
    r.set('value_set', 0)
    r.set('value_incr', 0)
    # r.delete('locker')

    processes = []
    for i in range(10):
        p = Process(target=process, args=(i,))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    print('value_set = {}'.format(r.get('value_set')))
    print('value_incr = {}'.format(r.get('value_incr')))


if __name__ == '__main__':
    main()
