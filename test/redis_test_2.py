import redis
import redis_lock
from multiprocessing import Process


r = redis.StrictRedis(host='localhost', port=6379, db=0)


def process():
    for i in range(10000):
        lock_set = redis_lock.Lock(r, 'value_set')
        lock_incr = redis_lock.Lock(r, 'value_incr')
        if lock_set.acquire():
            value = r.get('value_set')
            r.set('value_set', int(value) + 1)
            lock_set.release()
        if lock_incr.acquire():
            r.incr('value_incr')
            lock_incr.release()


def main():
    r.set('value_set', 0)
    r.set('value_incr', 0)

    processes = []
    for i in range(10):
        p = Process(target=process)
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    print('value_set = {}'.format(r.get('value_set')))
    print('value_incr = {}'.format(r.get('value_incr')))


if __name__ == '__main__':
    main()
